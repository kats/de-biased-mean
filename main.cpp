#include <string>
#include <iostream>
#include <vector>
#include <random>
#include <algorithm>

#define MINN 1
#define MAXN 100
#define NCLASS 10
#define NMEAS 100
#define NTESTS 10000
// investigation of means

// 1. arithmetic real  
// 2. arithmetic (biased)
// 3. arithmetic (de-biased)
// 4. median
typedef uint64_t TUint;
typedef std::vector<TUint> TClasses; 
typedef std::vector<TUint> TMeasurements;

void warmup(std::default_random_engine& generator,
            std::uniform_int_distribution<TUint>& distribution){
  for ( uint i = 0; i < 10; ++i ){
    distribution(generator);
  }
}

TUint fillClasses( TClasses& classes, TUint& nClasses ) {
  std::default_random_engine generator;
  std::uniform_int_distribution<TUint> distribution(MINN,MAXN);
  warmup(generator,distribution);
  nClasses = NCLASS;
  TUint totalsum = 0.0;
  for ( TUint i = 0; i < nClasses; ++i ) {
    classes.push_back(distribution(generator));
    totalsum += classes.back();
  }
  
//   std::cout << "Classes: ";
//   for ( TUint i = 0; i < nClasses; ++i ) {
//     std::cout << classes[i] << " ";
//   }
//   std::cout << std::endl;
  
  return totalsum;
}  

struct Measurements {
  Measurements ( TUint totalsum) { 
      generator = std::default_random_engine();
      distribution = std::uniform_int_distribution<TUint> (1,totalsum);
      warmup(generator,distribution);
      nMeasurements = NMEAS;
    };
  void clear() { measurements.clear(); };  
  void makeMeasurements (const TClasses& classes, TUint nClasses, TUint totalsum );
  TUint nMeasurements;
  TMeasurements measurements;
  std::default_random_engine generator;
  std::uniform_int_distribution<TUint> distribution;
};

void Measurements::makeMeasurements(const TClasses& classes, TUint nClasses, TUint totalsum)
{
  for ( TUint im = 0; im < nMeasurements; ++im ) {
    TUint number = distribution(generator);
    // which class?
    uint iclass;
    for ( iclass = 0; iclass < nClasses; ++iclass ) {
      if ( number <= classes[iclass] ) {
        break;
      } 
      number -= classes[iclass];
    }
    // measurement done
    measurements.push_back(classes[iclass]);
  }
  
//   std::cout << "Measurements: ";
//   for ( TUint i = 0; i < nMeasurements; ++i ) {
//     std::cout << measurements[i] << " ";
//   }
//   std::cout << std::endl;
  
}

double realmean( const TClasses& classes, TUint nClasses ) {
  double total = 0;
  for ( TUint i = 0; i < nClasses; ++i ) {
    total += classes[i];
  }
  return total/nClasses;
}

double biasedmean( const Measurements& measurement ){
  double total = 0;
  for ( TUint i = 0; i < measurement.nMeasurements; ++i ) {
    total += measurement.measurements[i];
  }
  return total/measurement.nMeasurements;
  
}

double debiasedmean( const Measurements& measurement ){
  double total = 0;
  for ( TUint i = 0; i < measurement.nMeasurements; ++i ) {
    total += 1.0/measurement.measurements[i];
  }
  return measurement.nMeasurements/total;
}

double median( const Measurements& measurement ){
  TMeasurements meas = measurement.measurements;
  std::sort(meas.begin(),meas.end());
  return meas[measurement.nMeasurements/2];
}

int main(int argc, char **argv) {
  
  
  TUint nClasses;
  TClasses classes;
 
  TUint 
    totalsum = fillClasses(classes,nClasses);
  std::cout << "Total: " << totalsum << std::endl;
  double
    rmean = realmean(classes,nClasses);
  std::cout << "Real Mean:     " << rmean << std::endl;
  
  uint nmeans = 2;
  std::vector<double> 
    smin(nmeans,totalsum),
    smax(nmeans,0.0),
    smean(nmeans,0.0),
    smad(nmeans,0.0);
  uint ntests = NTESTS;  
  Measurements measurement(totalsum);
  for ( uint itest = 0; itest < ntests; ++itest ) {
 
    measurement.makeMeasurements(classes,nClasses,totalsum);
    
    std::vector<double>
      means;
    means.push_back(biasedmean(measurement));
    means.push_back(debiasedmean(measurement));
//     means.push_back(median(measurement));
    // statistic
    for ( uint i = 0; i < nmeans; ++i ) {
      smin[i] = std::min(smin[i],means[i]-rmean);
      smax[i] = std::max(smax[i],means[i]-rmean);
      smean[i] += (means[i]-rmean)/ntests;
      smad[i] += std::abs(means[i]-rmean)/ntests;
    }
    
//     std::cout << "--- Means ------------- " << std::endl;
//     std::cout << "Biased Mean:   " << means[0] << std::endl;
//     std::cout << "DeBiased Mean: " << means[1] << std::endl;
// //     std::cout << "Median:        " << means[2] << std::endl;
    measurement.clear();
  }
  
  std::cout << "--- Statistics ------------- " << std::endl;
  std::cout << "---------- MIN ---------- MAX ----------- MEAN ---------- MAD ---"<< std::endl;
  std::cout << "Biased:   " << smin[0] << "  \t " << smax[0] << "    \t " << smean[0] << "    \t " << smad[0] << std::endl;
  std::cout << "DeBiased: " << smin[1] << "  \t " << smax[1] << "    \t " << smean[1] << " \t " << smad[1] << std::endl;
//   std::cout << "Median:   " << smin[2] << "    \t " << smax[2] << "    \t " << smean[2] << std::endl;
  
  return 0;
}

